# Databricks notebook source
# MAGIC %scala
# MAGIC org.apache.hadoop.fs.FileSystem.closeAll

# COMMAND ----------

dbutils.fs.ls("/")

# COMMAND ----------

dbutils.fs.ls("s3://srihasr-data1")

# COMMAND ----------

dbutils.credentials.assumeRole("arn:aws:iam::707343435239:role/srihasr-datarole1","arn:aws:iam::707343435239:role/srihasr-datarole2")

# COMMAND ----------

dbutils.credentials.assumeRole("arn:aws:iam::707343435239:role/srihasr-datarole1")


# COMMAND ----------

dbutils.credentials.assumeRole("arn:aws:iam::707343435239:role/srihasr-datarole2")

# COMMAND ----------

dbutils.credentials.assumeRole("arn:aws:iam::707343435239:role/srihasr-metarole")

# COMMAND ----------

dbutils.credentials.showCurrentRole()

# COMMAND ----------

spark.conf.set("spark.databricks.s3a.assumedRole","arn:aws:iam::707343435239:role/srihasr-datarole2") 

# COMMAND ----------

spark.conf.get("spark.databricks.s3a.assumedRole")

# COMMAND ----------

# MAGIC %pip install dp_utils

# COMMAND ----------

# MAGIC %pip install awscli

# COMMAND ----------

# MAGIC %sh
# MAGIC aws s3api list-buckets --query Owner.ID

# COMMAND ----------

dbutils.fs.put("/FileStore/init/s3debug.sh","""

  #!/bin/bash
if [[ $DB_IS_DRIVER = "TRUE" ]]; then
LOG4J_PATH="/home/ubuntu/databricks/spark/dbconf/log4j/driver/log4j.properties"
else
LOG4J_PATH="/home/ubuntu/databricks/spark/dbconf/log4j/executor/log4j.properties"
fi
echo "log4j.logger.com.amazonaws.request=DEBUG" >> ${LOG4J_PATH}
echo "log4j.logger.com.amazonaws.latency=DEBUG" >> ${LOG4J_PATH}

  exit 0 
""", True)

# COMMAND ----------

